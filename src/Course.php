<?php
namespace Nsru\Training;

class Course
{
    private $id;
    private $name;
    private $major_organization_id;
    private $major_organization_name;
    private $type_courses_id;
    private $type_courses_name;
    private $date_train;
    private $time_start_train;
    private $time_end_train;
    private $date_start_regis;
    private $date_end_regis;
    private $img;
    private $total_price;
    private $member;

    public function __get($name)
    {
        return $this->{$name};
    }

    public function import($data) {
        $this->id                       = $data->id;
        $this->name                     = $data->name;
        $this->major_organization_id    = $data->major_organization_id;
        $this->major_organization_name  = $data->major_organization_name;
        $this->type_courses_id          = $data->type_courses_id;
        $this->type_courses_name        = $data->type_courses_name;
        $this->date_train               = $data->date_train;
        $this->time_start_train         = $data->time_start_train;
        $this->time_end_train           = $data->time_end_train;
        $this->date_start_regis         = $data->date_start_regis;
        $this->date_end_regis           = $data->date_end_regis;
        $this->img                      = $data->img;
        $this->total_price              = $data->total_price;
    }

    public static function Now() {
        $url = "https://training.nsru.ac.th/api/data/course_now";

        $ch = \curl_init($url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        \curl_setopt($ch, CURLOPT_TIMEOUT,10);
        \curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $outputJson = \curl_exec($ch);
        $httpcode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $courses = [];
        if($httpcode == 200) {
            $output = \json_decode($outputJson);
            foreach($output as $courseData) {
                $course = new Course();
                $course->import($courseData);
                $courses[] = $course;
            }
        }

        return $courses;
    }
}